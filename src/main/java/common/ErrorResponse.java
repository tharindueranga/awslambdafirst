package common;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class ErrorResponse {

    private int status;
    private Exception exception;
    private String message;
    private String errorType;

    public void throwTheException(int status, Exception e){
        try {
            Map<String, Object> errorPayload = new HashMap<>();

            errorPayload.put("status", status);
            errorPayload.put("errorType", e.getCause());
            errorPayload.put("message",e.getMessage());
            String errorMessage = new ObjectMapper().writeValueAsString(errorPayload);

            throw new RuntimeException(errorMessage);

        }catch (JsonProcessingException j){
            j.printStackTrace();
        }
    }

    public void createAndThrowException(int status, String message, String errorType){
        try {
            Map<String, Object> errorPayload = new HashMap<>();
            errorPayload.put("status", status);
            errorPayload.put("errorType", errorType);
            errorPayload.put("message", message);
            String errorMessage = new ObjectMapper().writeValueAsString(errorPayload);

            throw new RuntimeException(errorMessage);

        } catch (JsonProcessingException j) {
            j.printStackTrace();
        }
    }

}
