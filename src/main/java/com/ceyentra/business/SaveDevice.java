package com.ceyentra.business;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.ceyentra.business.responses.ResponseObject;
import com.ceyentra.model.DeviceRequest;
import com.ceyentra.model.DeviceResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import common.ClientInitializer;
import common.ErrorResponse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SaveDevice implements RequestHandler<DeviceRequest,ResponseObject> {

    private DynamoDB dynamoDb;
    private String DYNAMODB_TABLE_NAME = "Person";
    private DynamoDBMapper mapper ;

    @Override
    public ResponseObject handleRequest(DeviceRequest input, Context context) {
        this.initDynamoDbClient();
        return this.saveDevice(input);
    }

    private void initDynamoDbClient() {
        AmazonDynamoDBClient amazonDynamoDBClient = new ClientInitializer()
                .initClient(this.dynamoDb, this.mapper);

        this.dynamoDb=new DynamoDB(amazonDynamoDBClient);
        this.mapper=new DynamoDBMapper(amazonDynamoDBClient);
    }

    /**
     * @param input the request data of the item which will save into the database
     * @returns the response in string
     */
    private ResponseObject saveDevice(DeviceRequest input) {

        Table table = dynamoDb.getTable(DYNAMODB_TABLE_NAME);
        ResponseObject<String> responseObject=new ResponseObject<>();
        Item searchedItem = table.getItem("name", input.getName());
        try {
            if (searchedItem == null) {

                final Item item = new Item()
                        .withString("name", input.getName())
                        .withDouble("count", input.getCount());

                table.putItem(item);
                responseObject.setStatus(201);

                responseObject.setContents("Saved new device");

            } else {

                BigDecimal count = (BigDecimal) searchedItem.get("count");
                BigDecimal newBig = count.add(new BigDecimal(input.getCount()));
                String valueString = newBig.toString();
                int newCount = Integer.parseInt(valueString);

                final Item item = new Item()
                        .withString("name", input.getName())
                        .withDouble("count", newCount);

                table.putItem(item);

                responseObject.setStatus(201);

                responseObject.setContents("Updated the device");

            }

        }catch (Exception e){
            new ErrorResponse().throwTheException(500,e);
        }

        return responseObject;
    }

}
