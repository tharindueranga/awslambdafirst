package com.ceyentra.business.responses;

public class ResponseObject<T> {

    private int status;

    private T contents;

    public ResponseObject() {}

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public T getContents() {
        return contents;
    }

    public void setContents(T contents) {
        this.contents = contents;
    }
}
