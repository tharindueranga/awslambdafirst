package com.ceyentra.business;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.ceyentra.business.responses.ResponseObject;
import com.ceyentra.model.DeviceResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import common.ClientInitializer;
import common.ErrorResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * this provides all the items of the defined table of database
 */
public class GetAllDevices implements RequestHandler<String,ResponseObject> {

    private DynamoDB dynamoDb;
    private String DYNAMODB_TABLE_NAME = "Person";
    private Regions REGION = Regions.US_WEST_2;
    private DynamoDBMapper mapper ;
    private AmazonDynamoDBClient client;

    private void initDynamoDbClient() {
        this.client = new ClientInitializer()
                .initClient(this.dynamoDb, this.mapper);

        this.dynamoDb=new DynamoDB(client);
        this.mapper=new DynamoDBMapper(client);
    }

    @Override
    public ResponseObject handleRequest(String s, Context context) {
        this.initDynamoDbClient();
        return this.getAllDevices();
    }

    /**
     * @returns all the items from database
     */
    private ResponseObject getAllDevices() {
        ResponseObject<List<DeviceResponse>> responseObject = new ResponseObject<>();

        Table table = dynamoDb.getTable(DYNAMODB_TABLE_NAME);

        ScanRequest scanRequest = new ScanRequest(DYNAMODB_TABLE_NAME);
        ArrayList<DeviceResponse> deviceResponses = new ArrayList<>();
        Map<String, AttributeValue> exclusiveStartKey = null;
        try {
            do {
                final ScanResult scanResult = client.scan(scanRequest);
                List<Map<String, AttributeValue>> items = scanResult.getItems();

                for (int i = 0; i < items.size(); i++) {
                    Map<String, AttributeValue> map = items.get(i);

                    AttributeValue value = map.get("name");
                    String name = value.getS();

                    int count = Integer.parseInt(map.get("count").getN());

                    DeviceResponse deviceResponse = new DeviceResponse();
                    deviceResponse.setName(name);
                    deviceResponse.setCount(count);

                    deviceResponses.add(deviceResponse);
                }

                exclusiveStartKey = scanResult.getLastEvaluatedKey();

                // Reusing same request object, just setting the start key
                scanRequest.setExclusiveStartKey(exclusiveStartKey);
            } while (exclusiveStartKey != null);

        }catch (Exception e){
            new ErrorResponse().throwTheException(500,e);
        }

        responseObject.setStatus(200);
        responseObject.setContents(deviceResponses);
        return responseObject;

    }
}
