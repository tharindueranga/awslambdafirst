package com.ceyentra.business;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.kinesis.model.ResourceNotFoundException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.ceyentra.business.responses.ResponseObject;
import com.ceyentra.model.NumberRequest;
import com.ceyentra.model.NumberResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import common.ClientInitializer;
import common.ErrorResponse;

import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.*;

/**
 * This is for getting the relevant number object
 */
public class GetNumber implements RequestHandler<NumberRequest,ResponseObject> {

    private DynamoDB dynamoDb;
    private String DYNAMODB_TABLE_NAME = "Number";
    private Regions REGION = Regions.US_WEST_2;
    private DynamoDBMapper mapper ;
    private final static String RESPONSE_404="404 Not Found: ";

    @Override
    public ResponseObject handleRequest(NumberRequest numberRequest, Context context) {
        this.initdynamoDbClient();
        return this.getNumber(numberRequest,context);
    }

    /**
     * @param numberRequest:this gets the request object
     * @param context:
     * @returns the response put into the response object
     */
    private ResponseObject getNumber(NumberRequest numberRequest,Context context) {

        Table number = dynamoDb.getTable(DYNAMODB_TABLE_NAME);
        Item item = number.getItem("id", numberRequest.getId());
        ResponseObject<NumberResponse> responseObject=new ResponseObject<>();
        NumberResponse numberResponse=new NumberResponse();
        if(item!=null){
            numberResponse.setId(numberRequest.getId());
            numberResponse.setCount(item.getInt("count"));
        }else {
            new ErrorResponse().createAndThrowException(404,"The Requested Number Not Found !","ResourceNotFoundException");
        }

        responseObject.setStatus(200);
        responseObject.setContents(numberResponse);
        return responseObject;
    }


    private void initdynamoDbClient(){
        AmazonDynamoDBClient amazonDynamoDBClient = new ClientInitializer()
                .initClient(this.dynamoDb, this.mapper);

        this.dynamoDb=new DynamoDB(amazonDynamoDBClient);
        this.mapper=new DynamoDBMapper(amazonDynamoDBClient);
    }
}
