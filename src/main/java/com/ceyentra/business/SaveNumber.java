package com.ceyentra.business;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.ceyentra.business.responses.ResponseObject;
import com.ceyentra.model.NumberRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import common.ClientInitializer;
import common.ErrorResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is use to save a new number item to the dynamodb
 */
public class SaveNumber implements RequestHandler<NumberRequest,ResponseObject> {

    private DynamoDB dynamoDb;
    private String DYNAMODB_TABLE_NAME = "Number";
    private Regions REGION = Regions.US_WEST_2;
    private DynamoDBMapper mapper ;

    @Override
    public ResponseObject handleRequest(NumberRequest numberRequest, Context context) {
        this.initDynamoDbClient();
        return this.saveNumber(numberRequest);
    }

    /**
     * @param numberRequest: this gets the request object
     * @return this return the response in string
     */
    private ResponseObject saveNumber(NumberRequest numberRequest) {
        Table table = dynamoDb.getTable(DYNAMODB_TABLE_NAME);
        ResponseObject<String> responseObject=new ResponseObject<>();

        try {
            final Item item = new Item()
                    .withInt("id", numberRequest.getId())
                    .withInt("count", numberRequest.getCount());

            table.putItem(item);
            responseObject.setStatus(201);

            responseObject.setContents("saved new number");

        }catch (Exception e){
            new ErrorResponse().throwTheException(500,e);
        }

        return responseObject;

    }

    /**
     * this method is used to initialize the dynamodb client
     */
    private void initDynamoDbClient() {
        AmazonDynamoDBClient amazonDynamoDBClient = new ClientInitializer()
                .initClient(this.dynamoDb, this.mapper);

        this.dynamoDb=new DynamoDB(amazonDynamoDBClient);
        this.mapper=new DynamoDBMapper(amazonDynamoDBClient);
    }

}
