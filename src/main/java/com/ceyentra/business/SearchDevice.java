package com.ceyentra.business;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.ceyentra.business.responses.ResponseObject;
import com.ceyentra.model.DeviceRequest;
import com.ceyentra.model.DeviceResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import common.ClientInitializer;
import common.ErrorResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * this class is used to search a unique device
 */
public class SearchDevice implements RequestHandler<DeviceRequest, ResponseObject> {

    private DynamoDB dynamoDb;
    private final String DYNAMODB_TABLE_NAME = "Person";
    private Regions REGION = Regions.US_WEST_2;
    private DynamoDBMapper mapper ;

    public SearchDevice() {
    }

    @Override
    public ResponseObject handleRequest(DeviceRequest deviceRequest, Context context) {
        this.initDynamoDbClient();
        return searchData(deviceRequest,context);
    }

    private void initDynamoDbClient() {
        AmazonDynamoDBClient amazonDynamoDBClient = new ClientInitializer()
                .initClient(this.dynamoDb, this.mapper);

        this.dynamoDb=new DynamoDB(amazonDynamoDBClient);
        this.mapper=new DynamoDBMapper(amazonDynamoDBClient);
    }

    /**
     * @param input: this gets the request object
     * @param context:
     * @returns searched device
     */
    private ResponseObject searchData(DeviceRequest input,Context context){
        Table table = dynamoDb.getTable(DYNAMODB_TABLE_NAME);
        ResponseObject<DeviceResponse>responseObject=new ResponseObject<>();

            Item searchedItem = table.getItem("name", input.getName());
            DeviceResponse deviceResponse = new DeviceResponse();
            if (searchedItem != null) {
                String name = searchedItem.getString("name");
                int count = searchedItem.getInt("count");
                deviceResponse.setName(name);
                deviceResponse.setCount(count);
            } else {
                new ErrorResponse().createAndThrowException(404,"The Requested Device Not Found !","ResourceNotFoundException");
            }

            responseObject.setStatus(200);
            responseObject.setContents(deviceResponse);

//        }catch (Exception e){
//             new ErrorResponse().throwTheException(500,e);
//        }

        return responseObject;
    }

}
