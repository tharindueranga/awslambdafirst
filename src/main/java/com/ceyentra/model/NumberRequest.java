package com.ceyentra.model;

public class NumberRequest {
    private int id;
    private int count;

    public NumberRequest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
