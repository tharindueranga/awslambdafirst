package com.ceyentra.model;

/**
 *
 */
public class NumberResponse {
    private int id;
    private int count;
    private String error;

    public NumberResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
