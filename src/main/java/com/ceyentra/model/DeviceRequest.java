package com.ceyentra.model;

public class DeviceRequest {
    private String name;
    private int count;

    public DeviceRequest() {
    }

    // standard getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
